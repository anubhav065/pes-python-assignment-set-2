print("%d is an integer" % 2)

print("%i is an integer" % 3)

print("%o is octal form of 10" % 10)

print("%x is hexadecimal form of 15" % 15)

print("%X is hexadecimal form of 14" % 14)

print("%e is exponential form of pi" % 3.1415926535897932384626433832795028841971693993751)

print("%f is a floating point number" % 12.678)

print("%c is an alphabet" % 'a')

print("%s is a string" % "'hello stranger'")
