from time import gmtime, strftime
import calendar
time_str = strftime("%H:%M:%S", gmtime())
time_str = time_str.split(':')
for i in range(len(time_str)):
    time_str[i] = int(time_str[i])
    
time_str[0] += 5
time_str[1] += 30

if time_str[1] > 59:
    time_str[0] += 1
    time_str[1] = time_str[1] - 60
    if time_str[0] >= 24:
        time_str[0] = time_str[0] - 24
        
print(strftime("%Y-%m-%d", gmtime()))
print("{}:{}:{}".format(time_str[0],time_str[1],time_str[2]))

print(calendar.month(2020, 1))
