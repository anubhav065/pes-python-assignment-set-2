dict1 = {'apple': 1, 'orange': 2, 'banana': 3}
dict2 = {'arun': 250, 'varun': 150, 'shyam': 100}
dict3 = {'mi': 3, 'csk': 2, 'srh': 1}

print("First Dictionary {}\n".format(dict1))
print("Second Dictionary {}\n".format(dict2))
print("Third Dictionary {}\n".format(dict3))

if len(dict1) == len(dict2) == len(dict3):
    print("All are of equal length")
elif len(dict1) > len(dict2):
    if len(dict1) > len(dict3):
        print("dict1 has maximum number of elements")
        print("dict2 has minimum number of elements")
    else:
        print("dict3 has maximum number of elements")
        print("dict2 has minimum number of elements")
else:
    if len(dict2) > len(dict3):
        print("dict2 has maximum number of elements")
        print("dict1 has minimum number of elements")
    else:
        print("dict3 has maximum number of elements")
        print("dict1 has minimum number of elements")
        
print("Adding new elements to dict1 and dict2")

while(1):
    x = input('WOuld you like to add a new entry to 1st dictionary? y/n: ')
    if x.lower() == 'y':
        key = input('Enter a new key: ')
        if key in dict1.keys():
            print("This key already exists in this dictionary. Please enter a unique key\n")
            continue
        value = input('Enter a new value: ')
        dict1.update({key: value})
    elif x.lower() == 'n':
        break
    else:
        print('Please enter a valid entry\n')

while(1):
    x = input('WOuld you like to add a new entry to 2nd dictionary? y/n: ')
    if x.lower() == 'y':
        key = input('Enter a new key: ')
        if key in dict2.keys():
            print("This key already exists in this dictionary. Please enter a unique key\n")
            continue
        value = input('Enter a new value: ')
        dict2.update({key: value})
    elif x.lower() == 'n':
        break
    else:
        print('Please enter a valid entry\n')

while(1):
    x = input('WOuld you like to add a new entry to 3rd dictionary? y/n: ')
    if x.lower() == 'y':
        key = input('Enter a new key: ')
        if key in dict3.keys():
            print("This key already exists in this dictionary. Please enter a unique key\n")
            continue
        value = input('Enter a new value: ')
        dict3.update({key: value})
    elif x.lower() == 'n':
        break
    else:
        print('Please enter a valid entry\n')

print("Updated dictionaries dict1, dict2, and dic3 are\n")
print(dict1)
print(dict2)
print(dict3)

print("\nlength of First Dictionary is {}".format(len(dict1)))
print("length of Second Dictionary is {}".format(len(dict2)))
print("length of Third Dictionary is {}\n".format(len(dict3)))


print("Concatenated dictionaries are\n")
print(str(dict1)+str(dict2)+str(dict3))