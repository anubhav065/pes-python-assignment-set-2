list_ = []

while True:
    check = input("Do you want to enter a number to the list or stop? Press y/n: ")
    if check == 'n':
        break
    elif check == 'y':
        try:
            list_.append(int(input("Enter a number: ")))
        except ValueError:
            print("This is not a number! Please try again!")
    else:
        print("Enter a valid option")
    
for i in range(0, len(list_)-1):
    for j in range(i+1, len(list_)):
        if list_[i] > list_[j]:
            temp = list_[i]
            list_[i] = list_[j]
            list_[j] = temp
            
print("Sorted numbers are {}".format(list_))