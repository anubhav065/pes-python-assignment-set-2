from datetime import datetime, timedelta
from time import sleep
import time
start = time.time()
i = 0
while i<= 12:
    current_time = datetime.now()+timedelta(minutes = 30, hours = 5)
    print(current_time.strftime("%H:%M:%S"))
    i += 1 
    time.sleep(5)
end = time.time()
print("time taken is {} seconds".format(round(end - start,2)))