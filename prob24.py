# \newline
print("Using newline \\n character")
print("line1\nline2\nline3")

print("#####################")

# \\
print("Using escape character to print a back slash")
print("\\")

print("#####################")

# \'
print("Using escape character to print a single quote mark")
print("\'")

print("#####################")

# \"
print("Using escape character to print a double quote mark")
print("\"")

print("#####################")

# \b
print("Using \b as backspace to remove 'ello' from 'hello world'")
print("hello\b\b\b\b world")

print("#####################")

# \t
print("Using escape character to add a horizontal tab between two words")
print("hello\tworld")

print("#####################")

# \v
print("Using escape character to add a vertical tab between two words")
print("hello\vworld")

print("#####################")

# \ooo
print("Using octal ASCII values to print Hello World")
print("\110\145\154\154\157\40\127\157\162\154\144\41")

print("#####################")

# \xhh
print("Using hexadecimal ASCII values to print Hello World")
print("\x48\x65\x6c\x6c\x6f\x20\x57\x6f\x72\x6c\x64\x21")
