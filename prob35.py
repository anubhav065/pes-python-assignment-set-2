tup1 = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' , 'Sunday')
tup2 = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec')

print("Dsiplaying days in a week")
for i in tup1:
    print(i)
    
print('\n')

print("Displaying months in a year")
for i in tup2:
    print(i)

print('\n')
    
tup3 = (1,2,3,4,5,6)
tup4 = (2,3,5,7,11)
tup5 = (1,4,9,16,25,36,19)
print("tup3 contains {}".format(tup3))
print("tup4 contains {}".format(tup4))
print("tup5 contains {}".format(tup5))

if len(tup3) == len(tup4) == len(tup5):
    print("All are of equal length")
elif len(tup3) > len(tup4):
    if len(tup3) > len(tup5):
        print("tup3 has maximum number of elements")
        print("tup4 has minimum number of elements")
    else:
        print("tup5 has maximum number of elements")
        print("tup4 has minimum number of elements")
else:
    if len(tup4) > len(tup5):
        print("tup4 has maximum number of elements")
        print("tup3 has minimum number of elements")
    else:
        print("tup5 has maximum number of elements")
        print("tup3 has minimum number of elements")
        
#Cannot delete elements from a tuple because they are immutable

x = input("Enter a new element that will be appended to tup3: ")
list_ = list(tup3)
if x.isnumeric():
    list_.append(int(x))
else:
    list_.append(x)
print(tuple(list_))
