from math import sqrt
from random import *
try:
    number = float(input("Please enter a number to be rounded: "))
except ValueError:
    print("Please enter a floating point number")
else:
    print(round(number,0))
    
try:
    number = float(input("Please enter a number you want to fin square root of: "))
except ValueError:
    print("Please enter a number")
else:
    print(sqrt(number))
    
print("generating a random number between 0 and 1")
print(random())

print("generating a random number between 50 and 100")
print(uniform(50,100))
    
