string = input("Enter a string to check if all characters are in lowercase: ")

#The islower() method returns True if all alphabets in a string are lowercase
#alphabets. If the string contains at least one uppercase alphabet, it returns False.

print(string.islower())

string = input("Enter a string to check if all characters are in uppercase: ")
#The isupper() method returns True if all alphabets in a string are uppercase
#alphabets. If the string contains at least one lowercase alphabet, it returns False.

print(string.isupper())

#The isspace() method returns True if there are only whitespace characters in the
#string. If not, it return False.

string = input("Enter a string to check if it only has whitespace characters in it: ")
print(string.isspace())

#The string lower() method converts all uppercase characters in a string into
#lowercase characters and returns it.

string = input("Enter a string in uppercase which will be returned as lowercase: ")
print(string.lower())

#The string upper() method converts all lowercase characters in a string into
#uppercase characters and returns it.
string = input("Enter a string in lowercase which will be returned as uppercase: ")
print(string.upper())