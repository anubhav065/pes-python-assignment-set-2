list1 = [20,30,50,10,40]
list2 = [2,3,13,7,11,5]
list3 = [2,4,6,8,10]

print("1st list is {}".format(list1))
print("2nd list is {}".format(list2))
print("3rd list is {}".format(list3))

print('\n')

maxlist = []
minlist = []

maxlist.append(max(list1))
number = max(list1)
list1 = [i for i in list1 if i!=number]
maxlist.append(max(list1))
number = max(list1)
list1 = [i for i in list1 if i!=number]

maxlist.append(max(list2))
number = max(list2)
list2 = [i for i in list2 if i!=number]
maxlist.append(max(list2))
number = max(list2)
list2 = [i for i in list2 if i!=number]

maxlist.append(max(list3))
number = max(list3)
list3 = [i for i in list3 if i!=number]
maxlist.append(max(list3))
number = max(list3)
list3 = [i for i in list3 if i!=number]

minlist.append(min(list1))
number = min(list1)
list1 = [i for i in list1 if i!=number]
minlist.append(min(list1))
number = min(list1)
list1 = [i for i in list1 if i!=number]

minlist.append(min(list2))
number = min(list2)
list2 = [i for i in list2 if i!=number]
minlist.append(min(list2))
number = min(list2)
list2 = [i for i in list2 if i!=number]

minlist.append(min(list3))
number = min(list3)
list3 = [i for i in list3 if i!=number]
minlist.append(min(list3))
number = min(list3)
list3 = [i for i in list3 if i!=number]

print("maxlist: {}".format(maxlist))
print("Avearge of maxlist is {}".format(sum(maxlist)//len(maxlist)))

print("minlist: {}".format(minlist))
print("Avearge of minlist is {}".format(sum(minlist)//len(minlist)))


