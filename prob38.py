dict1 ={'Name':'Ramakrishna','Age':25}
dict2={'EmpId':1234,'Salary':5000}

print("First Dictionary is {}".format(dict1))
print("Second Dictionary is {}".format(dict2))

print("Merigng dict1 and dict2")
newdict = {}
newdict.update(dict1)
newdict.update(dict2)

print(newdict)

print("Updating salary...")
newdict['Salary'] = newdict['Salary'] + 0.1*newdict['Salary']

print("Updating Age...")
newdict['Age'] = 26

print("Adding new key and value...")
newdict.update({'grade': 'B1'})

print("The keys and values in the Dictionary are\n")
for i in newdict:
    print(i, newdict[i])
    
print("\nRemoving the key 'Age'")
newdict.pop('Age')

print("Current condition of Dictionary is\n")
print(newdict)
