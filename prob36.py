tup1 = (10,20,30,40,50,60)
tup2 = ('a','b','c','d','e')

print("1st tuple contains all integer elements: {}".format(tup1))
print("2nd tuple contains all string elements: {}".format(tup2))

while(1):
	x = input("Do you want to check the index of an element in 1st tuple? y/n: ")
	if x.lower() == 'y':
		y = int(input("Enter the element whose index you want to check: "))
		try:
			print("index of {} in 1st tuple is {}".format(y,tup1.index(y)))
		except ValueError:
			print('{} is not in 1st tuple'.format(y))
	elif x.lower() == 'n':
		break
	else:
		print('Please enter a valid entry\n')

while(1):
	x = input("Do you want to check the index of an element in 2nd tuple? y/n: ")
	if x.lower() == 'y':
		y = input("Enter the element whose index you want to check: ")
		try:
			print("index of {} in 1st tuple is {}".format(y,tup2.index(y)))
		except ValueError:
			print('{} is not in 1st tuple'.format(y))
	elif x.lower() == 'n':
		break
	else:
		print('Please enter a valid entry\n')

while(1):
	x = input("Do you want to check the occurence of an element in 1st tuple? y/n: ")
	if x.lower() == 'y':
		y = int(input("Enter the element whose count you want to check: "))
		print('{} occured {} times in 1st tuple'.format(y,tup1.count(y)))
	elif x.lower() == 'n':
		break
	else:
		print('Please enter a valid entry\n')

while(1):
	x = input("Do you want to check the occurence of an element in 2nd tuple? y/n: ")
	if x.lower() == 'y':
		y = input("Enter the element whose count you want to check: ")
		print('{} occured {} times in 1st tuple'.format(y,tup2.count(y)))
	elif x.lower() == 'n':
		break
	else:
		print('Please enter a valid entry\n')