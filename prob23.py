import math

try:
    radius = float(input("Enter the radius"))
except ValueError:
    print("Please enter a number")
else:
    print("area of circle with radius {} is {}".format(int(radius), math.pi*(radius**2)))