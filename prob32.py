list1 = []
list2 = []
list3 = []

i = 0

print("Add 5 city names to first list: ")
while i<5:
    x = input("Enter city #{}: ".format(i+1))
    if x.isalpha():
        list1.append(x)
        i+=1
    else:
        print('This is not a valid city name. Please try again')
        continue

print('\n')
i=0

print("Add 5 city names to second list: ")
while i<5:
    x = input("Enter city #{}: ".format(i+1))
    if x.isalpha():
        list2.append(x)
        i+=1
    else:
        continue

print('\n')
i=0

print("Add 5 city names to third list: ")
while i<5:
    x = input("Enter city #{}: ".format(i+1))
    if x.isalpha():
        list3.append(x)
        i+=1
    else:
        continue

print('\n')

for i in list1:
    print('length of city #{} in 1st list is: {}'.format(list1.index(i), len(i)))

print('\n')

for i in list2:
    print('length of city #{} in 1st list is: {}'.format(list2.index(i), len(i)))

print('\n')

for i in list3:
    print('length of city #{} in 1st list is: {}'.format(list3.index(i), len(i)))

print('\n')

max_list1= len(list1[0])
min_list1 = len(list1[0])

for i in list1:
    if max_list1 < len(i):
        max_list1 = len(i)
    if min_list1 > len(i):
        min_list1 = len(i)


print("Maximum and minimum length of city names in first list are {}, {}".format(max_list1, min_list1))

max_list2 = len(list2[0])
min_list2 = len(list2[0])

for i in list2:
    if max_list2 < len(i):
        max_list2 = len(i)
    if min_list2 > len(i):
        min_list2 = len(i)

print("Maximum and minimum length of city names in second list are {}, {}".format(max_list2, min_list2))

max_list3= len(list3[0])
min_list3 = len(list3[0])

for i in list3:
    if max_list3 < len(i):
        max_list3 = len(i)
    if min_list3 > len(i):
        min_list3 = len(i)

print("Maximum and minimum length of city names in third list are {}, {}".format(max_list3, min_list3))

print("maximum length of a city in all the three lists is: {}".format(max([max_list1, max_list2, max_list3])))
print("minimum length of a city in all the three lists is: {}".format(min([min_list1, min_list2, min_list3])))
        
print("Removing first and last element in each list...")
list1.pop(0)
list1.pop(-1)
list2.pop(0)
list2.pop(-1)
list3.pop(0)
list3.pop(-1)
print("Resulting lists are: ")
print(list1, list2, list3)