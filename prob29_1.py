string = input("Enter a string: ")

#The capitalize() function returns a string with first letter capitalized. It doesn't modify the old string.
#If the first letter of a string is an uppercase letter or a non-alphabetic character, it returns the original string.
print("turn first character of string to uppercase")
print(string.capitalize())

print('\n')

#The center() method returns a string padded with specified fillchar. It doesn't modify the original string.
print("pad the string with # character")
print(string.center(24, '#'))

print('\n')

#count() method returns the number of occurrences of the substring in the given string.
print("checking for the frequency of the word 'tri' in your string")
print(string.count('tri'))

print('\n')

#The endswith() method returns a boolean.
#It returns True if strings ends with the specified suffix.
#It returns False if string doesn't end with the specified suffix.
print("checking if your string ends with 'ring'")
print(string.endswith('ring'))

print('\n')

#The find() method returns the lowest index of the substring (if found). If not found, it returns -1.
print("returning lowest index of the substring 'str' in your string")
print(string.find('str'))