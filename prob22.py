import math

den = int(input("Enter the denominator"))
#rad2 = int(input("Enter the second value"))

print("sine of pi/denominator")
print(math.sin(math.pi/den))

print("cosine of pi/denominator")
print(math.cos(math.pi/den))

print("cotangent of pi/denominator")
print(math.tan(math.pi/den))

print("arc sine of pi/denominator")
print(math.asin(math.pi/den))

print("arc cosine of pi/denominator")
print(math.acos(math.pi/den))

print("arc cotangent of pi/denominator")
print(math.atan(math.pi/den))
