list_= []

i = 0

print("Add 5 city names to first list: ")
while i<5:
    x = input("Enter city #{}: ".format(i+1))
    if x.isalpha():
        list_.append(x)
        i+=1
    else:
    	print('This is not a valid city name. Please try again')
        continue

while(1):
	new_city = input("Enter a new city that should be appended to the list: ")
	if new_city.isalpha():
		if new_city in list_:
			print('This city is already in the list. Please try again:')
		else:
			list_.append(new_city)
			break
	else:
		print('This is not a valid city name. Please try again')

while(1):
	new_city = input("Enter a new city that will be added to the list at index number 4: ")
	if new_city.isalpha():
		if new_city in list_:
			print('This city is already in the list. Please try again:')
		else:
			list_.insert(4,new_city)
			break
	else:
		print('This is not a valid city name. Please try again')

print("Sorting list by default: ")
print(sorted(list_))

print("Sorting list in descending order: ")
print(sorted(list_, reverse = True))

print(list_)

print("Deleting last three elements...")
list_.pop(-1)
list_.pop(-1)
list_.pop(-1)
print("Current cities in the list are {}".format(list_))