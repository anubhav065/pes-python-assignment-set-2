string = input("Enter a string: ")

#The string format() method formats the given string into a nicer output in Python.
print("'{}' string displayed using format function".format(string))

#The index() method returns the index of a substring inside the string (if found).
#If the substring is not found, it raises an exception.
print('\n')
substring = input("Enter the substring whose starting index you would like to find in the string you entered: ")
print('searching for starting index of substring "test" in your string')
try:
    print("index is at {}".format(string.index(substring)))
except ValueError:
    print("Sorry! Unable to find the substring")
    
#The isalnum() method returns True if all characters in the string are alphanumeric
#(either alphabets or numbers). If not, it returns False.
print("\n")
if input("Would you like to check if your string has only alphaets and numbers? y/n: ").lower() == 'y':
	print(string.isalnum())

if input("Would you like to check if your string has only alphaets? y/n: ").lower() == 'y':
	print(string.isalpha())

if input("Would you like to check if your string has only numbers? y/n: ").lower() == 'y':
	print(string.isdecimal())